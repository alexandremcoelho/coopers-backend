import express from "express";
import { createConnection } from "typeorm";
import RouterBuilder from "./api/routers";
import databaseConfig from "./config/database";
import cors from "cors";

const app = express();

app.use(
  cors({
    origin: "*",
  })
);
app.use(express.json());

const PORT = process.env.PORT || 5000;

RouterBuilder(app);

createConnection(databaseConfig)
  .then((_conection) => {
    app.listen(PORT, () => {
      console.log(`Running at http://localhost:${PORT}`);
    });
  })
  .catch((err) => {
    process.exit(1);
  });
