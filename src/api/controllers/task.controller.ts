import { Request, Response } from "express";
import { Task } from "../../entities";
import { getRepository } from "typeorm";

export default class TaskController {
  create = async (req: Request, res: Response) => {
    const taskRepository = getRepository(Task);
    const { label } = req.body;
    const task = new Task(label);
    const createdtask = await taskRepository.save(task);

    res.status(201).send(createdtask);
  };

  list = async (req: Request, res: Response) => {
    const taskRepository = getRepository(Task);
    const tasks: Array<Task> = await taskRepository.find();

    res.send(tasks);
  };

  edit = async (req: Request, res: Response) => {
    const taskRepository = getRepository(Task);
    const task_id = Number(req.params["task_id"]);
    const { label } = req.body;
    let task: any = await taskRepository.findOne({ id: task_id });

    if (!task) {
      return res.status(404).send({
        detail: "Not found.",
      });
    }

    task.label = label;

    await taskRepository.save(task);

    res.status(200).send(task);
  };

  change = async (req: Request, res: Response) => {
    const taskRepository = getRepository(Task);
    const task_id = Number(req.params["task_id"]);
    let task: any = await taskRepository.findOne({ id: task_id });

    if (!task) {
      return res.status(404).send({
        detail: "Not found.",
      });
    }

    task.changeStatus();

    await taskRepository.save(task);

    res.status(200).send(task);
  };

  delete = async (req: Request, res: Response) => {
    const taskRepository = getRepository(Task);
    const task_id = Number(req.params["task_id"]);

    const task = await taskRepository.findOne({ id: task_id });
    if (!task) {
      return res.status(404).send({
        detail: "Not found.",
      });
    }

    await taskRepository.delete({ id: task_id });

    res.sendStatus(204);
  };

  clear = async (req: Request, res: Response) => {
    const taskRepository = getRepository(Task);

    await taskRepository.clear();

    res.sendStatus(204);
  };
}
