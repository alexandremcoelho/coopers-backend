import { Request, Response } from "express";
import { Message } from "../../entities";
import { getRepository } from "typeorm";
import nodemailer from "nodemailer";

export default class TaskController {
  create = async (req: Request, res: Response) => {
    const messageRepository = getRepository(Message);
    const { name, email, telephone, message } = req.body;
    const newmessage = new Message(name, email, telephone, message);
    const createdMessage = await messageRepository.save(newmessage);

    let transporter = nodemailer.createTransport({
      host: "smtp-mail.outlook.com",
      port: 587,
      secure: false,
      auth: {
        user: "alexandremcteste@outlook.com",
        pass: "123abcD#",
      },
    });
    let text = `<p>nome: ${name}<p>
    <p>email: ${email}<p>
    <p>telefone: ${telephone}<p>
    <p>mensagem: ${message}<p>`;

    transporter
      .sendMail({
        from: "alexandre <alexandremcteste@outlook.com>", // sender address
        to: "lincoln@coopers.pro", // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: text, // html body
      })
      .then((message) => {
        console.log(message);
      })
      .catch((err) => {
        console.log(err);
      });

    res.status(201).send(createdMessage);
  };

  list = async (req: Request, res: Response) => {
    const messageRepository = getRepository(Message);
    const messages: Array<Message> = await messageRepository.find();

    res.send(messages);
  };
}
