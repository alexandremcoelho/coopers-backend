import { Router } from "express";
import messageController from "../controllers/message.controller";
import { CheckMessageFields } from "../middlewares";

const router = Router();

export default (): Router => {
  const messagescontroller = new messageController();
  router.get("/messages", messagescontroller.list);
  router.post("/messages", CheckMessageFields, messagescontroller.create);

  return router;
};
