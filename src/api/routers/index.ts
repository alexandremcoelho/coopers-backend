import { Express } from "express";
import TaskRouter from "./tasks.router";
import MessageRouter from "./message.router";

export default (app: Express) => {
  const taskrouter = TaskRouter();
  const messagerouter = MessageRouter();

  app.use("/api", taskrouter);
  app.use("/api", messagerouter);
};
