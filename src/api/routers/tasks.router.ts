import { Router } from "express";
import TaskController from "../controllers/task.controller";
import { CheckTaskFields, UniqueTaskVerification } from "../middlewares";

const router = Router();

export default (): Router => {
  const taskcontroller = new TaskController();
  router.get("/tasks", taskcontroller.list);
  router.post(
    "/tasks",
    CheckTaskFields,
    UniqueTaskVerification,
    taskcontroller.create
  );
  router.patch(
    "/tasks/:task_id",
    CheckTaskFields,
    UniqueTaskVerification,
    taskcontroller.edit
  );
  router.post("/tasks/:task_id/changestatus", taskcontroller.change);
  router.delete("/tasks/:task_id", taskcontroller.delete);
  router.delete("/tasks", taskcontroller.clear);

  return router;
};
