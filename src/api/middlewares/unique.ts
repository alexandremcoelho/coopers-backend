import { Request, Response, NextFunction } from "express";
import { Task, Message } from "../../entities";
import { getRepository } from "typeorm";

export const UniqueTaskVerification = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { label } = req.body;
  const taskRepository = getRepository(Task);

  const task = await taskRepository.find({ label: label });

  if (task.length != 0) return res.sendStatus(422);

  return next();
};
