import { Request, Response, NextFunction } from "express";

export const CheckTaskFields = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const recievedFields = Object.keys(req.body);
  let allFields = ["label"];
  let missing = {};
  for (let i = 0; i < allFields.length; i++) {
    if (!recievedFields.includes(allFields[i])) {
      let missingField = allFields[i];
      missing = {
        ...missing,
        ...{ missingField: "Missing field " + missingField },
      };
    }
  }
  if (Object.keys(missing).length > 0) {
    res.status(400).send(missing);
  }
  return next();
};

export const CheckMessageFields = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const recievedFields = Object.keys(req.body);
  let allFields = ["name", "email", "telephone", "message"];
  let missing = {};
  for (let i = 0; i < allFields.length; i++) {
    if (!recievedFields.includes(allFields[i])) {
      let missingField = allFields[i];
      missing = {
        ...missing,
        ...{ missingField: "Missing field " + missingField },
      };
    }
  }
  if (Object.keys(missing).length > 0) {
    res.status(400).send(missing);
  }
  return next();
};
