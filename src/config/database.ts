import { ConnectionOptions } from "typeorm";

const config: ConnectionOptions = {
  type: "sqlite",
  database: `${__dirname}/../../database.sqlite`,
  entities: [`${__dirname}/../entities/*.ts`],
  synchronize: true,
  cli: {
    entitiesDir: `${__dirname}/../entities`,
    migrationsDir: `${__dirname}/../migrations`,
    subscribersDir: `${__dirname}/../subscribers`,
  },
};

export default config;
