import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column()
  telephone!: number;

  @Column()
  message!: string;

  constructor(name: string, email: string, telephone: number, message: string) {
    this.name = name;
    this.email = email;
    (this.telephone = telephone), (this.message = message);
  }
}
