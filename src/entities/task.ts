import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  label!: string;

  @Column()
  done!: boolean;

  constructor(label: string) {
    this.label = label;
    this.done = false;
  }

  changeStatus() {
    this.done = !this.done;
  }
}
