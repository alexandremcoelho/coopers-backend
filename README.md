# Coopers

backend dedicado ao teste técnico

# Rotas

## Sobre Tasks:

Esse endpoint é responsável por criar, editar e listar Tasks.

### `POST /api/tasks/`

```json
// REQUEST
{
  "label": "Dvelop the Todo list page"
}
```

```json
// RESPONSE STATUS -> HTTP 201
{
  "label": "Dvelop the Todo list page",
  "done": false,
  "id": 11
}
```

Caso o campo label nao seja enviado o retorno sera:

```json
// RESPONSE STATUS -> HTTP 400
{
  "missingField": "Missing field label"
}
```

Caso haja a tentativa de criação de uma label já cadastrada o sistema irá responder com `HTTP 422 - Unprocessable Entity`.

### `Get /api/tasks/` - Rota que lista todas as Tasks

```json
// RESPONSE STATUS -> 200 OK
[
  {
    "label": "Dvelop the Todo list page",
    "done": false,
    "id": 13
  },
  {
    "label": "Create the drag-and-drop function",
    "done": false,
    "id": 14
  },
  {
    "label": "add new tasks",
    "done": false,
    "id": 15
  }
]
```

### `Patch /api/tasks/<task_id>` - Rota que atualiza a Tasks

```json
// REQUEST
{
  "label": "task legal"
}
```

```json
// RESPONSE STATUS -> HTTP 200
{
  "label": "task legal",
  "done": false,
  "id": 16
}
```

Caso seja passado uma task_id inválida, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
// RESPONSE STATUS -> HTTP 404 NOT FOUND

{
  "detail": "Not found."
}
```

### `Delete /api/tasks/<task_id>` - Rota que deleta a Tasks

```json
// RESPONSE STATUS -> HTTP 204

```

Caso seja passado uma task_id inválida, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
// RESPONSE STATUS -> HTTP 404 NOT FOUND

{
  "detail": "Not found."
}
```

### `post /api/tasks/<task_id>/changestatus/` - Rota que altera o status da task

```json
// RESPONSE STATUS -> HTTP 200

{
  "label": "task",
  "done": true,
  "id": 16
}
```

Caso seja passado uma task_id inválida, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
// RESPONSE STATUS -> HTTP 404 NOT FOUND

{
  "detail": "Not found."
}
```

## Sobre Messages:

Esse endpoint é responsável por criar e listar Messages.

### `POST /api/Messages/`

```json
// REQUEST
{
  "name": "alexandre",
  "email": "alexandre@outlook.com",
  "telephone": 45999343142,
  "message": "muito bom"
}
```

```json
// RESPONSE STATUS -> HTTP 201
{
  "name": "alexandre",
  "email": "alexandre@outlook.com",
  "telephone": 45999343142,
  "message": "muito bom",
  "id": 13
}
```

Caso algum campo nao seja enviado o retorno sera:

```json
// REQUEST
{
  "email": "alexandre@outlook.com",
  "telephone": 45999343142,
  "message": "muito bom"
}
```

```json
// RESPONSE STATUS -> HTTP 400
{
  "missingField": "Missing field name"
}
```

### `Get /api/Messages/`

```json
// RESPONSE STATUS -> 200 OK
[
  {
    "name": "alexandre",
    "email": "alexandre@outlook.com",
    "telephone": 54,
    "message": "muito bom",
    "id": 1
  }
]
```
